# Forth. Транслятор и модель

- Кулагин Иван P33151. 
- `forth | stack | harv | hw | instr | struct | stream | mem | pstr | prob1 | spi`
- С упрощением.

## Язык программирования

``` ebnf
program ::= term

term ::= command
        | var_init
        | string_init
        | push_symbol
        | term term

command ::= "NOP" | "MOD" | "+" | "<" | "NEGATE" | "INVERT" | "DUP" | "OVER" | "ROT" | "SWAP" | "DROP"  
            | "IF" | "ELSE" | "ENDIF" 
            | "WHILE" | "BEGIN" | "REPEAT"
            | READ | READ# | WR | WR#

push_symbol = [-2^31; 2^31], ["a"; "z"]

string_init = (string_name): string_value

var_init ::= var_name: var_value 
                | &string_name

```

Код выполняется последовательно. Операции:

- `OVER` -- ... `a b` >> ... `a b a` 
- `DUP` -- ... `a` >> ... `a a`
- `<` -- ... `a b` >> ... `a<b`
- `MOD` -- ... `a b` >> ... `a%b`
- `NEGATE` -- ... `a` >> ... `-a`
- `INVERT` -- ... `a` >> ... `(a+1)*-1`
- `+` -- ... `a b` >> ... `a+b`
- `DROP` -- ... `a b` >> ... `a`
- `ROT` -- ... `a b c` >> ... `b c a`
- `SWAP` -- ... `a b` >> ... `b a`
- `{num}` -- ... `a b` >> ... `a b {num}`
- `{char}` -- ... `a b` >> ... `a b {char}`
- `IF` -- возьмёт со стека значение, и если оно True, то перейдёт далее. Если False, то прыгнет на ELSE
- `ELSE` -- сработает в случае, если при IF на стеке лежало False
- `ENDIF` -- закрывает блок кода, принадлежащий IF
- `BEGIN` -- является меткой, для возвращения назад, когда мы в цикле
- `WHILE` -- возьмёт со стека значение, и если оно True, то перейдёт далее. Если False, то прыгнет на команду, на +1 дальше, чем REPEAT
- `REPEAT` -- возвращает на BEGIN в случае если во время WHILE было True 
- `NOP` -- заглушка. не выполняет никаких операций
- `READ` (чтение с прямой адресацией)-- берёт со стека значение `x` и кладёт на стек значение memory[x]
- `READ#` (чтение с косвенной адресацией) -- берёт со стека значение `x`, кладёт на стек значение memory[memory[x]], и увеличивает значение memory[x] на +1
- `WR` (запись с прямой адресацией) -- берёт со стека значение `x`, и значение `y` и записывает в память memory[x] = y
- `WR#` (запись с косвенной адресацией) -- берёт со стека значение `x`, и значение `y` и записывает в память memory[memory[x]] = y, и увеличивает значение memory[x] на +1


## Организация памяти
Модель памяти процессора:

1. Память команд. Машинное слово -- не определено. Реализуется списком словарей, описывающих инструкции (одно слово -- одна ячейка).
2. Память данных. Машинное слово -- 32 бита, знаковое. Линейное адресное пространство. Реализуется списком чисел.
3. Стек. Заменяет регистры. Машинное слово -- 32 бита, знаковое. Линейное адресное пространство. Реализуется списком чисел.

Строки, объявленные пользователем распеделяются по памяти один символ на ячейку.

```text
     Stack (first-in-last-out buffer)
+-----------------------------+
|    ...                      |
+-----------------------------+

     Instruction memory
+-----------------------------+
|    ...                      |
+-----------------------------+

      Data memory
+-----------------------------+
| 00  : input                 |
| 01  : output                |
| 03  : data                  |
|    ...                      |
+-----------------------------+
```

## Система команд

Особенности процессора:

- Машинное слово -- 32 бит, знаковое.
- `Память данных`:
    - адресуется через регистр `data_address`;
    - может быть записана:
        - с макушки стека;
        - с буффера ввода
    - может быть прочитана:
        - в буффер вывода;
- `Память команд`:
    - адресуется через регистр `program`;
- `Стек`:
    - адресуется через регистр `head`;
    - Верхушка стека (верхние 3 элемента):
        - может быть записана:
            - из буфера;
        - может быть прочитана:
            - на вход АЛУ
            - на вывод
        - используется как флаг `is_true` -1 - на верхушке `True`, иначе `False`
- Ввод-вывод -- stream.
- `program_counter` -- счётчик команд:
    - инкрементируется после каждой инструкции или перезаписывается инструкцией перехода.

### Набор инструкции

| Syntax         | Mnemonic        | Кол-во тактов | Comment                           |
|:---------------|:----------------|---------------|:----------------------------------|
| `OVER`         | OVER            | 1             | ... `a b` >> ... `a b a`          |
| `DUP`          | DUP             | 1             | ... `a` >> ... `a a`              |
| `<`            | LT              | 2             | `a b` >> ... `a<b`                |
| `MOD`          | MOD             | 2             | ... `a b` >> ... `a%b`            |
| `NEGATE`       | NEG             | 2             | ... `a` >> ... `-a`               |
| `INVERT`       | INV             | 2             | ... `0` >> ... `-1`               |
| `+`            | PLUS            | 2             | ... `a b` >> ... `a+b`            |
| `DROP`         | DROP            | 1             | ... `a b` >> ... `a`              |
| `ROT`          | ROT             | 1             | ... `a b c` >> ... `b c a`        |
| `SWAP`         | SWAP            | 1             | ... `a b` >> ... `b a`            |
| `{sym}`        | PUSH `{sym}`    | 1             | ... `a b` >> ... `a b {sym}`      |
| `IF`           | JNT {ELSE + 1}  | 1             | см. язык                          |
| `ELSE`         | JMP {ENDIF}     | 1             | см. язык                          |
| `ENDIF`        | NOP             | 1             | см. язык                          |
| `BEGIN`        | BEGIN           | 1             | см. язык                          |
| `WHILE`        | JNT {REPEAT + 1}| 1             | см. язык                          |
| `REPEAT`       | JMP {BEGIN + 1} | 1             | см. язык                          |
| `NOP`          | NOP             | 1             | см. язык                          |
| `WR`           | WR_DIR          | 2             | см. язык                          |
| `WR#`          | WR_NDR          | 6             | см. язык                          |
| `READ`         | READ_DIR        | 2             | см. язык                          |
| `READ#`        | READ_NDR        | 7             | см. язык                          |


### Кодирование инструкций

- Машинный код сериализуется в список JSON.
- Один элемент списка, одна инструкция.
- Индекс списка -- адрес инструкции. Используется для команд перехода.

Пример:

```json
[
   {
        "opcode": "PUSH",
        "arg": "0",
        "term": [
            1,
            "PUSH",
            "0"
        ]
    }
]
```

где:

- `opcode` -- строка с кодом операции;
- `arg` -- аргумент (может отсутствовать);
- `term` -- информация о связанном месте в исходном коде (если есть).

Типы данные в модуле [isa](isa.py), где:

- `Opcode` -- перечисление кодов операций;
- `Term` -- структура для описания значимого фрагмента кода исходной программы.

## Транслятор

Интерфейс командной строки: `translator.py <input_file> <target_file> <data_section_file>"`

Реализовано в модуле: [translator](translator.py)

Этапы трансляции (функция `translate`):
1. Трансформирование текста в последовательность значимых термов.
    - Переменные:
        - Транслируются в соответствующие значения на этапе трансляции.
        - Задаётся либо числовым значением, либо указателем на начало строки (используя &string_name)
2. Проверка корректности программы (одинаковое количество IF, ELSE, ENDIF и BEGIN, WHILE, REPEAT).
3. Генерация машинного кода.

Правила генерации машинного кода:

- одно слово языка -- одна инструкция;
- для команд, однозначно соответствующих инструкциям -- прямое отображение;
- для циклов с соблюдением парности (многоточие -- произвольный код):

    | Номер команды/инструкции | Программа | Машинный код |
    |:-------------------------|:----------|:-------------|
    | n                        | `BEGIN`   | `BEGIN`      |
    | ...                      | ...       | ...          |
    | n+3                      | `WHILE`   | `JNT (k+1)`  |
    | ...                      | ...       | ...          |
    | k                        | `REPEAT`  |  `JMP (n+1)` |
    | k+1                      | ...       | ...          |
- для условных операторов (многоточие -- произвольный код):

    | Номер команды/инструкции | Программа | Машинный код |
    |:-------------------------|:----------|:-------------|
    | n                        | `IF`      | `JNT n+4`    |
    | ...                      | ...       | ...          |
    | n+3                      | `ELSE`    | `JMP (k+1)`  |
    | ...                      | ...       | ...          |
    | k                        | `ENDIF`   |  `NOP`       |
    | k+1                      | ...       | ...          |

## Модель процессора

Интерфейс командной строки: `machine.py <machine_code_file> <input_file>`

Реализовано в модуле: [machine](./machine.py).

![machine](img/machine.png)

Сигналы (обрабатываются за один такт, реализованы в виде методов класса Memory):

- `wr` -- записать в data memory
- `oe` -- прочитать из data memory 
- `latch_data_address` -- защёлкнуть значение в `data_address`

### Memory

Реализована в классе `Memory`.

`data_memory` -- однопортовая память, поэтому либо читаем, либо пишем.

Сигналы (обрабатываются за один такт, реализованы в виде методов класса):

- `latch_data_address` -- защёлкнуть выбранное значение в `data_address`;
- `get_instruction` -- получить инструкцию из памяти команд (`program_memory`);
- `wr` -- записать выбранное значение в память:
    - инкрементированное;
    - декрементированное;
    - с порта ввода `input` (обработка на Python):
        - извлечь из входного буфера значение и записать в память;
        - если буфер пуст -- выбросить исключение;
- `output` -- записать аккумулятор в порт вывода (обработка на Python).

### DataPath

Реализован в классе `DataPath`.

![datapath](img/datapath.png)

Сигналы (обрабатываются за один такт, реализованы в виде методов класса):

- `push` -- записать на макушку стека значение из АЛУ, input или argument
- `oe_stack` -- прочитать из макушки стека и отправить в output
- `latch_data_head` -- защёлкнуть значение в `data_head`

Флаги:

- `is_true` -- лежит ли на стеке true.

### АЛУ

![alu](img/alu.png)

Сигналы (обрабатываются за один такт, реализованы в виде методов класса):

- `operation_sel` -- произвести операцию

### ControlUnit

![control unit](img/cu.png)

Реализован в классе `ControlUnit`.

- Hardwired (реализовано полностью на python).
- Моделирование на уровне инструкций.
- Трансляция инструкции в последовательность (0-7 тактов) сигналов: `decode_and_execute`.

Сигнал:

- `latch_program_couter` -- сигнал для обновления счётчика команд в control_unit.

Особенности работы модели:

- Для журнала состояний процессора используется стандартный модуль logging.
- Количество инструкций для моделирования ограничено hardcoded константой.
- Остановка моделирования осуществляется при помощи исключений:
    - `StopIteration` -- если выполнена инструкция `halt`.
- Управление симуляцией реализовано в функции `simulate`.

## Тестирование

В качестве тестов использовано 3 алгоритма:

1. [hello world](examples/hello_world_code).
2. [cat](examples/cat_code) -- программа `cat`, повторяем ввод на выводе.
3. [prob1](examples/prob1_code) -- программа, решающая 1 проблему Эйлера.


Пример использования и журнал работы процессора на примере `cat`:

``` console
> cat examples/cat_code
BEGIN
    #in
    READ
    #out
    WR
    -1
    WHILE
REPEAT
> cat examples/cat_input
Lorem ipsum
> ./translator.py examples/cat_code examples/cat_code.out examples/data_section
source LoC: 8 code instr: 9
> cat examples/cat_code.out
[
    {
        "opcode": "BEGIN",
        "term": [
            1,
            "BEGIN",
            null
        ]
    },
    {
        "opcode": "PUSH",
        "arg": 0,
        "term": [
            2,
            "PUSH",
            0
        ]
    },
    {
        "opcode": "READ_DIR",
        "term": [
            3,
            "READ",
            null
        ]
    },
    {
        "opcode": "PUSH",
        "arg": 1,
        "term": [
            4,
            "PUSH",
            1
        ]
    },
    {
        "opcode": "WRITE_DIR",
        "term": [
            5,
            "WR",
            null
        ]
    },
    {
        "opcode": "PUSH",
        "arg": "-1",
        "term": [
            6,
            "PUSH",
            "-1"
        ]
    },
    {
        "opcode": "JNT",
        "arg": 8,
        "term": [
            7,
            "WHILE",
            null
        ]
    },
    {
        "opcode": "JMP",
        "arg": 1,
        "term": [
            8,
            "REPEAT",
            null
        ]
    },
    {
        "opcode": "HALT",
        "term": [
            9,
            "HALT",
            null
        ]
    }
]
> ./machine.py machine_code.out examples/data_section examples/cat_input
DEBUG:root:{TICK: 1, PC: 1, HEAD: 0, TOS: 0, 0, 0}  Opcode.PUSH 0 ('0' @ 2:PUSH)
DEBUG:root:{TICK: 2, PC: 2, HEAD: 1, TOS: 0, 0, 0}  Opcode.READ_DIR  ('None' @ 3:READ)
DEBUG:root:{TICK: 3, PC: 2, HEAD: 0, TOS: 0, 0, 0}  Opcode.READ_DIR  ('None' @ 3:READ)
INFO:root:input: o,r,e,m, ,i,p,s,u,m << L
DEBUG:root:{TICK: 4, PC: 3, HEAD: 1, TOS: 0, 0, 76}  Opcode.PUSH 1 ('1' @ 4:PUSH)
DEBUG:root:{TICK: 5, PC: 4, HEAD: 2, TOS: 0, 76, 1}  Opcode.WR_DIR  ('None' @ 5:WR)
DEBUG:root:{TICK: 6, PC: 4, HEAD: 2, TOS: 0, 76, 1}  Opcode.WR_DIR  ('None' @ 5:WR)
DEBUG:root:output:  << L
DEBUG:root:{TICK: 7, PC: 5, HEAD: 0, TOS: 0, 0, 0}  Opcode.PUSH -1 ('-1' @ 6:PUSH)
DEBUG:root:{TICK: 8, PC: 6, HEAD: 1, TOS: 0, 0, -1}  Opcode.JNT 8 ('None' @ 7:WHILE)
DEBUG:root:{TICK: 9, PC: 7, HEAD: 0, TOS: 0, 0, 0}  Opcode.JMP 1 ('None' @ 8:REPEAT)
DEBUG:root:{TICK: 10, PC: 1, HEAD: 0, TOS: 0, 0, 0}  Opcode.PUSH 0 ('0' @ 2:PUSH)
DEBUG:root:{TICK: 11, PC: 2, HEAD: 1, TOS: 0, 0, 0}  Opcode.READ_DIR  ('None' @ 3:READ)
DEBUG:root:{TICK: 12, PC: 2, HEAD: 0, TOS: 0, 0, 0}  Opcode.READ_DIR  ('None' @ 3:READ)
INFO:root:input: r,e,m, ,i,p,s,u,m << o
DEBUG:root:{TICK: 13, PC: 3, HEAD: 1, TOS: 0, 0, 111}  Opcode.PUSH 1 ('1' @ 4:PUSH)
DEBUG:root:{TICK: 14, PC: 4, HEAD: 2, TOS: 0, 111, 1}  Opcode.WR_DIR  ('None' @ 5:WR)
DEBUG:root:{TICK: 15, PC: 4, HEAD: 2, TOS: 0, 111, 1}  Opcode.WR_DIR  ('None' @ 5:WR)
DEBUG:root:output: L << o
DEBUG:root:{TICK: 16, PC: 5, HEAD: 0, TOS: 0, 0, 0}  Opcode.PUSH -1 ('-1' @ 6:PUSH)
DEBUG:root:{TICK: 17, PC: 6, HEAD: 1, TOS: 0, 0, -1}  Opcode.JNT 8 ('None' @ 7:WHILE)
DEBUG:root:{TICK: 18, PC: 7, HEAD: 0, TOS: 0, 0, 0}  Opcode.JMP 1 ('None' @ 8:REPEAT)
DEBUG:root:{TICK: 19, PC: 1, HEAD: 0, TOS: 0, 0, 0}  Opcode.PUSH 0 ('0' @ 2:PUSH)
DEBUG:root:{TICK: 20, PC: 2, HEAD: 1, TOS: 0, 0, 0}  Opcode.READ_DIR  ('None' @ 3:READ)
DEBUG:root:{TICK: 21, PC: 2, HEAD: 0, TOS: 0, 0, 0}  Opcode.READ_DIR  ('None' @ 3:READ)
INFO:root:input: e,m, ,i,p,s,u,m << r
DEBUG:root:{TICK: 22, PC: 3, HEAD: 1, TOS: 0, 0, 114}  Opcode.PUSH 1 ('1' @ 4:PUSH)
DEBUG:root:{TICK: 23, PC: 4, HEAD: 2, TOS: 0, 114, 1}  Opcode.WR_DIR  ('None' @ 5:WR)
DEBUG:root:{TICK: 24, PC: 4, HEAD: 2, TOS: 0, 114, 1}  Opcode.WR_DIR  ('None' @ 5:WR)
DEBUG:root:output: L,o << r
DEBUG:root:{TICK: 25, PC: 5, HEAD: 0, TOS: 0, 0, 0}  Opcode.PUSH -1 ('-1' @ 6:PUSH)
DEBUG:root:{TICK: 26, PC: 6, HEAD: 1, TOS: 0, 0, -1}  Opcode.JNT 8 ('None' @ 7:WHILE)
DEBUG:root:{TICK: 27, PC: 7, HEAD: 0, TOS: 0, 0, 0}  Opcode.JMP 1 ('None' @ 8:REPEAT)
DEBUG:root:{TICK: 28, PC: 1, HEAD: 0, TOS: 0, 0, 0}  Opcode.PUSH 0 ('0' @ 2:PUSH)
DEBUG:root:{TICK: 29, PC: 2, HEAD: 1, TOS: 0, 0, 0}  Opcode.READ_DIR  ('None' @ 3:READ)
DEBUG:root:{TICK: 30, PC: 2, HEAD: 0, TOS: 0, 0, 0}  Opcode.READ_DIR  ('None' @ 3:READ)
INFO:root:input: m, ,i,p,s,u,m << e
DEBUG:root:{TICK: 31, PC: 3, HEAD: 1, TOS: 0, 0, 101}  Opcode.PUSH 1 ('1' @ 4:PUSH)
DEBUG:root:{TICK: 32, PC: 4, HEAD: 2, TOS: 0, 101, 1}  Opcode.WR_DIR  ('None' @ 5:WR)
DEBUG:root:{TICK: 33, PC: 4, HEAD: 2, TOS: 0, 101, 1}  Opcode.WR_DIR  ('None' @ 5:WR)
DEBUG:root:output: L,o,r << e
DEBUG:root:{TICK: 34, PC: 5, HEAD: 0, TOS: 0, 0, 0}  Opcode.PUSH -1 ('-1' @ 6:PUSH)
DEBUG:root:{TICK: 35, PC: 6, HEAD: 1, TOS: 0, 0, -1}  Opcode.JNT 8 ('None' @ 7:WHILE)
DEBUG:root:{TICK: 36, PC: 7, HEAD: 0, TOS: 0, 0, 0}  Opcode.JMP 1 ('None' @ 8:REPEAT)
DEBUG:root:{TICK: 37, PC: 1, HEAD: 0, TOS: 0, 0, 0}  Opcode.PUSH 0 ('0' @ 2:PUSH)
DEBUG:root:{TICK: 38, PC: 2, HEAD: 1, TOS: 0, 0, 0}  Opcode.READ_DIR  ('None' @ 3:READ)
DEBUG:root:{TICK: 39, PC: 2, HEAD: 0, TOS: 0, 0, 0}  Opcode.READ_DIR  ('None' @ 3:READ)
INFO:root:input:  ,i,p,s,u,m << m
DEBUG:root:{TICK: 40, PC: 3, HEAD: 1, TOS: 0, 0, 109}  Opcode.PUSH 1 ('1' @ 4:PUSH)
DEBUG:root:{TICK: 41, PC: 4, HEAD: 2, TOS: 0, 109, 1}  Opcode.WR_DIR  ('None' @ 5:WR)
DEBUG:root:{TICK: 42, PC: 4, HEAD: 2, TOS: 0, 109, 1}  Opcode.WR_DIR  ('None' @ 5:WR)
DEBUG:root:output: L,o,r,e << m
DEBUG:root:{TICK: 43, PC: 5, HEAD: 0, TOS: 0, 0, 0}  Opcode.PUSH -1 ('-1' @ 6:PUSH)
DEBUG:root:{TICK: 44, PC: 6, HEAD: 1, TOS: 0, 0, -1}  Opcode.JNT 8 ('None' @ 7:WHILE)
DEBUG:root:{TICK: 45, PC: 7, HEAD: 0, TOS: 0, 0, 0}  Opcode.JMP 1 ('None' @ 8:REPEAT)
DEBUG:root:{TICK: 46, PC: 1, HEAD: 0, TOS: 0, 0, 0}  Opcode.PUSH 0 ('0' @ 2:PUSH)
DEBUG:root:{TICK: 47, PC: 2, HEAD: 1, TOS: 0, 0, 0}  Opcode.READ_DIR  ('None' @ 3:READ)
DEBUG:root:{TICK: 48, PC: 2, HEAD: 0, TOS: 0, 0, 0}  Opcode.READ_DIR  ('None' @ 3:READ)
INFO:root:input: i,p,s,u,m <<
DEBUG:root:{TICK: 49, PC: 3, HEAD: 1, TOS: 0, 0, 32}  Opcode.PUSH 1 ('1' @ 4:PUSH)
DEBUG:root:{TICK: 50, PC: 4, HEAD: 2, TOS: 0, 32, 1}  Opcode.WR_DIR  ('None' @ 5:WR)
DEBUG:root:{TICK: 51, PC: 4, HEAD: 2, TOS: 0, 32, 1}  Opcode.WR_DIR  ('None' @ 5:WR)
DEBUG:root:output: L,o,r,e,m <<
DEBUG:root:{TICK: 52, PC: 5, HEAD: 0, TOS: 0, 0, 0}  Opcode.PUSH -1 ('-1' @ 6:PUSH)
DEBUG:root:{TICK: 53, PC: 6, HEAD: 1, TOS: 0, 0, -1}  Opcode.JNT 8 ('None' @ 7:WHILE)
DEBUG:root:{TICK: 54, PC: 7, HEAD: 0, TOS: 0, 0, 0}  Opcode.JMP 1 ('None' @ 8:REPEAT)
DEBUG:root:{TICK: 55, PC: 1, HEAD: 0, TOS: 0, 0, 0}  Opcode.PUSH 0 ('0' @ 2:PUSH)
DEBUG:root:{TICK: 56, PC: 2, HEAD: 1, TOS: 0, 0, 0}  Opcode.READ_DIR  ('None' @ 3:READ)
DEBUG:root:{TICK: 57, PC: 2, HEAD: 0, TOS: 0, 0, 0}  Opcode.READ_DIR  ('None' @ 3:READ)
INFO:root:input: p,s,u,m << i
DEBUG:root:{TICK: 58, PC: 3, HEAD: 1, TOS: 0, 0, 105}  Opcode.PUSH 1 ('1' @ 4:PUSH)
DEBUG:root:{TICK: 59, PC: 4, HEAD: 2, TOS: 0, 105, 1}  Opcode.WR_DIR  ('None' @ 5:WR)
DEBUG:root:{TICK: 60, PC: 4, HEAD: 2, TOS: 0, 105, 1}  Opcode.WR_DIR  ('None' @ 5:WR)
DEBUG:root:output: L,o,r,e,m,  << i
DEBUG:root:{TICK: 61, PC: 5, HEAD: 0, TOS: 0, 0, 0}  Opcode.PUSH -1 ('-1' @ 6:PUSH)
DEBUG:root:{TICK: 62, PC: 6, HEAD: 1, TOS: 0, 0, -1}  Opcode.JNT 8 ('None' @ 7:WHILE)
DEBUG:root:{TICK: 63, PC: 7, HEAD: 0, TOS: 0, 0, 0}  Opcode.JMP 1 ('None' @ 8:REPEAT)
DEBUG:root:{TICK: 64, PC: 1, HEAD: 0, TOS: 0, 0, 0}  Opcode.PUSH 0 ('0' @ 2:PUSH)
DEBUG:root:{TICK: 65, PC: 2, HEAD: 1, TOS: 0, 0, 0}  Opcode.READ_DIR  ('None' @ 3:READ)
DEBUG:root:{TICK: 66, PC: 2, HEAD: 0, TOS: 0, 0, 0}  Opcode.READ_DIR  ('None' @ 3:READ)
INFO:root:input: s,u,m << p
DEBUG:root:{TICK: 67, PC: 3, HEAD: 1, TOS: 0, 0, 112}  Opcode.PUSH 1 ('1' @ 4:PUSH)
DEBUG:root:{TICK: 68, PC: 4, HEAD: 2, TOS: 0, 112, 1}  Opcode.WR_DIR  ('None' @ 5:WR)
DEBUG:root:{TICK: 69, PC: 4, HEAD: 2, TOS: 0, 112, 1}  Opcode.WR_DIR  ('None' @ 5:WR)
DEBUG:root:output: L,o,r,e,m, ,i << p
DEBUG:root:{TICK: 70, PC: 5, HEAD: 0, TOS: 0, 0, 0}  Opcode.PUSH -1 ('-1' @ 6:PUSH)
DEBUG:root:{TICK: 71, PC: 6, HEAD: 1, TOS: 0, 0, -1}  Opcode.JNT 8 ('None' @ 7:WHILE)
DEBUG:root:{TICK: 72, PC: 7, HEAD: 0, TOS: 0, 0, 0}  Opcode.JMP 1 ('None' @ 8:REPEAT)
DEBUG:root:{TICK: 73, PC: 1, HEAD: 0, TOS: 0, 0, 0}  Opcode.PUSH 0 ('0' @ 2:PUSH)
DEBUG:root:{TICK: 74, PC: 2, HEAD: 1, TOS: 0, 0, 0}  Opcode.READ_DIR  ('None' @ 3:READ)
DEBUG:root:{TICK: 75, PC: 2, HEAD: 0, TOS: 0, 0, 0}  Opcode.READ_DIR  ('None' @ 3:READ)
INFO:root:input: u,m << s
DEBUG:root:{TICK: 76, PC: 3, HEAD: 1, TOS: 0, 0, 115}  Opcode.PUSH 1 ('1' @ 4:PUSH)
DEBUG:root:{TICK: 77, PC: 4, HEAD: 2, TOS: 0, 115, 1}  Opcode.WR_DIR  ('None' @ 5:WR)
DEBUG:root:{TICK: 78, PC: 4, HEAD: 2, TOS: 0, 115, 1}  Opcode.WR_DIR  ('None' @ 5:WR)
DEBUG:root:output: L,o,r,e,m, ,i,p << s
DEBUG:root:{TICK: 79, PC: 5, HEAD: 0, TOS: 0, 0, 0}  Opcode.PUSH -1 ('-1' @ 6:PUSH)
DEBUG:root:{TICK: 80, PC: 6, HEAD: 1, TOS: 0, 0, -1}  Opcode.JNT 8 ('None' @ 7:WHILE)
DEBUG:root:{TICK: 81, PC: 7, HEAD: 0, TOS: 0, 0, 0}  Opcode.JMP 1 ('None' @ 8:REPEAT)
DEBUG:root:{TICK: 82, PC: 1, HEAD: 0, TOS: 0, 0, 0}  Opcode.PUSH 0 ('0' @ 2:PUSH)
DEBUG:root:{TICK: 83, PC: 2, HEAD: 1, TOS: 0, 0, 0}  Opcode.READ_DIR  ('None' @ 3:READ)
DEBUG:root:{TICK: 84, PC: 2, HEAD: 0, TOS: 0, 0, 0}  Opcode.READ_DIR  ('None' @ 3:READ)
INFO:root:input: m << u
DEBUG:root:{TICK: 85, PC: 3, HEAD: 1, TOS: 0, 0, 117}  Opcode.PUSH 1 ('1' @ 4:PUSH)
DEBUG:root:{TICK: 86, PC: 4, HEAD: 2, TOS: 0, 117, 1}  Opcode.WR_DIR  ('None' @ 5:WR)
DEBUG:root:{TICK: 87, PC: 4, HEAD: 2, TOS: 0, 117, 1}  Opcode.WR_DIR  ('None' @ 5:WR)
DEBUG:root:output: L,o,r,e,m, ,i,p,s << u
DEBUG:root:{TICK: 88, PC: 5, HEAD: 0, TOS: 0, 0, 0}  Opcode.PUSH -1 ('-1' @ 6:PUSH)
DEBUG:root:{TICK: 89, PC: 6, HEAD: 1, TOS: 0, 0, -1}  Opcode.JNT 8 ('None' @ 7:WHILE)
DEBUG:root:{TICK: 90, PC: 7, HEAD: 0, TOS: 0, 0, 0}  Opcode.JMP 1 ('None' @ 8:REPEAT)
DEBUG:root:{TICK: 91, PC: 1, HEAD: 0, TOS: 0, 0, 0}  Opcode.PUSH 0 ('0' @ 2:PUSH)
DEBUG:root:{TICK: 92, PC: 2, HEAD: 1, TOS: 0, 0, 0}  Opcode.READ_DIR  ('None' @ 3:READ)
DEBUG:root:{TICK: 93, PC: 2, HEAD: 0, TOS: 0, 0, 0}  Opcode.READ_DIR  ('None' @ 3:READ)
INFO:root:input:  << m
DEBUG:root:{TICK: 94, PC: 3, HEAD: 1, TOS: 0, 0, 109}  Opcode.PUSH 1 ('1' @ 4:PUSH)
DEBUG:root:{TICK: 95, PC: 4, HEAD: 2, TOS: 0, 109, 1}  Opcode.WR_DIR  ('None' @ 5:WR)
DEBUG:root:{TICK: 96, PC: 4, HEAD: 2, TOS: 0, 109, 1}  Opcode.WR_DIR  ('None' @ 5:WR)
DEBUG:root:output: L,o,r,e,m, ,i,p,s,u << m
DEBUG:root:{TICK: 97, PC: 5, HEAD: 0, TOS: 0, 0, 0}  Opcode.PUSH -1 ('-1' @ 6:PUSH)
DEBUG:root:{TICK: 98, PC: 6, HEAD: 1, TOS: 0, 0, -1}  Opcode.JNT 8 ('None' @ 7:WHILE)
DEBUG:root:{TICK: 99, PC: 7, HEAD: 0, TOS: 0, 0, 0}  Opcode.JMP 1 ('None' @ 8:REPEAT)
DEBUG:root:{TICK: 100, PC: 1, HEAD: 0, TOS: 0, 0, 0}  Opcode.PUSH 0 ('0' @ 2:PUSH)
DEBUG:root:{TICK: 101, PC: 2, HEAD: 1, TOS: 0, 0, 0}  Opcode.READ_DIR  ('None' @ 3:READ)
DEBUG:root:{TICK: 102, PC: 2, HEAD: 0, TOS: 0, 0, 0}  Opcode.READ_DIR  ('None' @ 3:READ)
DEBUG:root:Input buffer is Empty
output: Lorem ipsum
instr_counter:  79 ticks: 102
```

```text
| ФИО          | алг.    | code инстр. | инстр. | такт. | вариант                                                   |
|--------------|---------|-------------|--------|-------|-----------------------------------------------------------|
| Кулагин И.А. | hello   | 14          | 118    | 222   | forth, stack, harv, hw, instr, struct, stream, mem, prob1 |
| Кулагин И.А. | cat     | 9           | 79     | 102   | forth, stack, harv, hw, instr, struct, stream, mem, prob1 |
| Кулагин И.А. | prob1   | 68          | 12281  | 16261 | forth, stack, harv, hw, instr, struct, stream, mem, prob1 |
```
